package se.experis.contact;

public class Contact {
    private String fullName;
    private String address;
    private String mobileNumber;
    private String workNumber;
    private String birthDate;

    public Contact(String fullName, String address, String mobileNumber, String workNumber, String birthDate) {
        this.fullName = fullName;
        this.address = address;
        this.mobileNumber = mobileNumber;
        this.workNumber = workNumber;
        this.birthDate = birthDate;
    }

    public String getFullName() {
        return fullName;
    }

    public String getAddress() {
        return address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public String getBirthDate() {
        return birthDate;
    }
}

