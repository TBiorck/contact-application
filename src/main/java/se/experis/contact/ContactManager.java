/*
    The ContactManages handles the contacts. When initialized, it loads contacts from a JSON file and stores
    them in a ArrayList. When new contacts are added, it writes the updated ArrayList to the JSON file.

    It also handles search for contacts.
 */

package se.experis.contact;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.util.ArrayList;


public class ContactManager {

    private ArrayList<Contact> contacts;
    private final String CONTACTS_FILE_PATH = ".\\data\\contacts.json";

    public ContactManager() {
        contacts = loadFromFile();
    }

    private ArrayList<Contact> loadFromFile() {
        Gson gson = new Gson();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(CONTACTS_FILE_PATH));
            ArrayList<Contact> contacts = gson.fromJson(reader, new TypeToken<ArrayList<Contact>>(){}.getType());
            if (contacts != null) {
                return contacts;
            }
            else {
                return new ArrayList<Contact>();
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void saveToFile() {
        Gson gson = new Gson();
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(CONTACTS_FILE_PATH));
            gson.toJson(contacts, writer);
            writer.flush();
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addContact(Contact contact) {
        contacts.add(contact);
        saveToFile();
    }

    public Iterable<Contact> getContacts() {
        return contacts;
    }

    // Returns an ArrayList of all contacts that has a field matching the search word.
    public ArrayList<Contact> search(String searchWord) {
        ArrayList<Contact> searchResult = new ArrayList<>();

        for (Contact contact : contacts) {
            if (searchWordMatchesAFieldValue(contact, searchWord)) {
                searchResult.add(contact);
            }
        }
        return searchResult;
    }

    /* Check each field of the contact class to see if it matches the search phrase. */
    private boolean searchWordMatchesAFieldValue(Contact contact, String searchWord) {
        searchWord = searchWord.toLowerCase();
        return contact.getFullName().toLowerCase().contains(searchWord)
                || contact.getAddress().toLowerCase().contains(searchWord)
                || contact.getMobileNumber().toLowerCase().contains(searchWord)
                || contact.getWorkNumber().toLowerCase().contains(searchWord)
                || contact.getBirthDate().toLowerCase().contains(searchWord);
    }
}
