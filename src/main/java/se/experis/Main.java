package se.experis;

import se.experis.contact.Contact;
import se.experis.contact.ContactManager;
import se.experis.view.Console;

import java.util.ArrayList;


public class Main {
    public static void main(String[] args) {
        Console console = new Console();
        ContactManager contactManager = new ContactManager();

        // Use if contacts.json file is empty
//        addSomeContacts(contactManager);


        /* MAIN LOOP */
        while (true) {
            console.printMainMenu();

            String searchWord = console.askForSearchWord();

            ArrayList<Contact> searchResult = contactManager.search(searchWord);

            if(searchResult.isEmpty()) {
                console.printNoResultsMessage();
            }
            else {
                for (Contact contact : searchResult) {
                    console.printSearchResult(contact.getFullName(), contact.getAddress(), contact.getMobileNumber(), contact.getWorkNumber(), contact.getBirthDate(), searchWord);
                }
            }

            console.pressKeyToContinue();
        }
    }

    /* USED TO FILL CONTACTS LIST IF EMPTY */
//    private static void addSomeContacts(ContactManager contactManager) {
//        contactManager.addContact(new Contact("Tim", "MyAddress", "123", "321", "010101"));
//        contactManager.addContact((new Contact("Rob", "Robs Road", "123456789", "987824558", "090909")));
//        contactManager.addContact((new Contact("Rob", "Robs Road", "123456789", "987824558", "090909")));
//        contactManager.addContact((new Contact("Nellie", "Nellie Alley", "879442274", "555555555", "080808")));
//        contactManager.addContact((new Contact("Mike", "Storgatan 5", "789189354", "444444444", "070707")));
//        contactManager.addContact((new Contact("Richard", "Storgatan 12", "456789123", "333333333", "060606")));
//        contactManager.addContact((new Contact("Peter", "Abbey road", "354128459", "", "050505")));
//        contactManager.addContact((new Contact("Magnus", "Farmer street", "963852741", "22222222", "040404")));
//        contactManager.addContact((new Contact("Sven", "Village road 2", "852741963", "111111111", "030303")));
//        contactManager.addContact((new Contact("Nicolaus", "North Pole 1a", "963852741", "000000000", "020202")));
//    }
}
