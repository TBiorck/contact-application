/*
    This class is responsible for handling output and input to the console.
 */


package se.experis.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {
    public final String ANSI_RESET = "\u001B[0m";
    public final String ANSI_GREEN = "\u001B[32m";

    public void printMainMenu() {
        System.out.println("\n\n\n\n\n\n\n\n--- CONTACT APP ---\n");
        System.out.println("-Press Ctrl+C to exit-\n\n");
    }

    public String askForSearchWord() {
        System.out.println("Search: ");
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            return reader.readLine();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Prints all fields of the Contact class. Fields containing the search word gets "highlighted" by changing its color.
    public void printSearchResult(String fullName, String address, String mobileNumber, String workNumber, String birthDate, String searchWord) {
        System.out.println("Name: " + fullName.toLowerCase().replace(searchWord, ANSI_GREEN + searchWord.toLowerCase() + ANSI_RESET)
                + "\nAddress: " + address.toLowerCase().replace(searchWord, ANSI_GREEN + searchWord.toLowerCase() + ANSI_RESET)
                + "\nMobile: " + mobileNumber.toLowerCase().replace(searchWord, ANSI_GREEN + searchWord.toLowerCase() + ANSI_RESET)
                + "\nWork: " + workNumber.toLowerCase().replace(searchWord, ANSI_GREEN + searchWord.toLowerCase() + ANSI_RESET)
                + "\nBirth date: " + birthDate.toLowerCase().replace(searchWord, ANSI_GREEN + searchWord.toLowerCase() + ANSI_RESET) + "\n");
    }

    public void pressKeyToContinue(){
        System.out.println("Press Enter key to continue...");
        try {
            System.in.read();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printNoResultsMessage() {
        System.out.println("No results..");
    }
}
